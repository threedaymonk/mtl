MTL Compiler
============

This is the Nabaztag MTL compiler released by Mindscape and modified by the
OpenJabNab project, extracted into a separate repository.

Usage
-----

    make

This will generate `mtl_comp`, the compiler, and `mtl_simu`, the simulator.
